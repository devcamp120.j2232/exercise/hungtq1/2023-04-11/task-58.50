package com.devcamp.task58_50.employeelist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58_50.employeelist.model.employee;

public interface EmployeeRepository extends JpaRepository<employee, Long>{
    
}
