package com.devcamp.task58_50.employeelist.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58_50.employeelist.model.employee;
import com.devcamp.task58_50.employeelist.repository.EmployeeRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CEmployeeController {
    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/employees")
    public ResponseEntity<List<employee>> getAllEmployees(){
        try {
            List<employee> listEmployees = new ArrayList<employee>();
            employeeRepository.findAll().forEach(listEmployees::add);
            return new ResponseEntity<>(listEmployees, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
